CC=gcc
CFLAGS = -Wall -g
LDFLAGS =

all: Editor

Editor: main.o input.o
	$(CC) $(CFLAGS) main.o input.o

main.o : main.c utils.h types.h dafs.h
input.o : input.c types.h

clean:
	rm -f Editor *.o

install: Editor
	cp Editor usr/bin/
	cmod 555 usr/bin/Editor 